#include <cinttypes>
#include <fstream>
#include <iostream>
#include <signal.h>
#include <string>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <unistd.h>

static volatile bool send_samples;

void intHandler(int dummy __attribute__((unused)))
{
    send_samples = false;
}

int main(int argc, char **argv)
{
    signal(SIGINT, intHandler);

    std::string filename;
    if (argc > 1)
        {
            filename = argv[1];
        }
    std::ifstream binaryfile(filename, std::ios::in | std::ios::binary);

    std::cout << "checking captured counter\n";
    uint32_t last_count = 0;
    uint32_t current_count = 0;
    long long unsigned int sample_position = 0;
    long long unsigned int packet_losses = 0;
    char buffer[4];
    bool first_count = true;
    send_samples = true;
    while (send_samples and !binaryfile.eof())
        {
            //binaryfile.read(&buffer[0], 4);
            binaryfile.read(reinterpret_cast<char *>(&current_count), sizeof(current_count));
            //std::cout<<"[0]: "<<(int)buffer[0]<<"[1]: "<<(int)buffer[1]<<"[2]: "<<(int)buffer[2]<<"[3]: "<<(int)buffer[3]<<std::endl;
            if (!first_count)
                {
                    long long int diff;
                    diff = (long long int)current_count - (long long int)last_count;
                    if (diff != 1 and !binaryfile.eof())
                        {
                            if (diff != -4294967295)
                                {
                                    std::cout << "Loss detected at [" << sample_position << "] : diff: " << diff << " curr:" << current_count << " last:" << last_count << std::endl;
                                    packet_losses++;
                                }
                        }
                }
            else
                {
                    first_count = false;
                }
            last_count = current_count;
            sample_position++;
        }
    std::cout << "Stopping...\n";
    std::cout << "Packet loss is " << (float)packet_losses * 100.0 / (float)sample_position << " %\n";
    return 0;
}
