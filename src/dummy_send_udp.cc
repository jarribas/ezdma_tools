/* -*- c++ -*- */
/*

 */
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/format.hpp>
#include <boost/lockfree/spsc_queue.hpp>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <queue>
#include <signal.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <thread>

static volatile bool send_samples;

std::mutex udp_thread_mutex;
std::condition_variable wake_udp_thread;

#define RX_PACKET_SIZE_BYTES 4096

class ezdma_packet
{
public:
    uint8_t rx_buf[RX_PACKET_SIZE_BYTES];
};

boost::lockfree::spsc_queue<ezdma_packet, boost::lockfree::capacity<256> >
    fifo_buff;

void intHandler(int dummy __attribute__((unused)))
{
    send_samples = false;
    wake_udp_thread.notify_one();
    std::cout << "Sent notification to stop all threads\n";
}

void send_udp_packet(boost::asio::ip::udp::socket *d_socket,
    boost::asio::ip::udp::endpoint d_endpoint)
{
    ssize_t r;
    ezdma_packet signal_samples;
    while (send_samples)
        {
            std::unique_lock<std::mutex> lk(udp_thread_mutex);
            wake_udp_thread.wait(
                lk, [] { return !fifo_buff.empty() or send_samples == false; });
            lk.unlock();
            if (send_samples == false) break;
            // send the UDP packet
            fifo_buff.pop(signal_samples);
            try
                {
                    // split the packet in four
                    for (int i = 0; i < 4; i++)
                        {
                            r = d_socket->send_to(
                                boost::asio::buffer((void *)&signal_samples.rx_buf[i * 1024], 1024),
                                d_endpoint);
                            if (r != 1024)
                                {
                                    std::cout << "UDP is not sending all the bytes!\n";
                                    break;
                                }
                        }
                }
            catch (std::exception &e)
                {
                    std::cout << boost::format("send error: %s") % e.what() << std::endl;
                    break;
                }
        }
}


int main(int argc, char **argv)
{
    signal(SIGINT, intHandler);

    // open the UDP socket
    boost::asio::ip::udp::socket *d_socket;
    boost::asio::ip::udp::endpoint d_endpoint;
    boost::asio::ip::udp::endpoint d_endpoint_rcvd;
    boost::asio::io_service d_io_service;
    std::string host("127.0.0.1");
    int port = 1234;
    std::string s_port = (boost::format("%d") % port).str();
    boost::asio::ip::udp::resolver resolver(d_io_service);
    boost::asio::ip::udp::resolver::query query(
        host, s_port, boost::asio::ip::resolver_query_base::passive);
    d_endpoint = *resolver.resolve(query);
    d_socket = new boost::asio::ip::udp::socket(d_io_service);
    d_socket->open(d_endpoint.protocol());
    boost::asio::socket_base::reuse_address roption(true);
    d_socket->set_option(roption);
    boost::asio::socket_base::broadcast option(true);
    d_socket->set_option(option);
    send_samples = true;

    // Prepare the sender thread
    // start pcap capture thread
    std::thread udp_thread;
    udp_thread = std::thread(&send_udp_packet, d_socket, d_endpoint);

    // Main loop
    int rx_bytes;
    ezdma_packet rx_packet;
    int16_t *sample_pointer = (int16_t *)&rx_packet.rx_buf[0];
    uint32_t last_count;
    bool first_packet = true;
    int packets_count = 0;

    int8_t counter = 0;
    int8_t max_count = 100;
    int bytes_per_complex_sample = 2;
    int n_channels = 2;
    while (send_samples == true)
        {
            int current_byte_position = 0;
            for (int n = 0;
                 n < RX_PACKET_SIZE_BYTES / (bytes_per_complex_sample * n_channels);
                 n++)
                {
                    for (int j = 0; j < n_channels; j++)
                        {
                            // 16 bits counter
                            // sample_pointer[current_byte_position]=counter;
                            // current_byte_position++;

                            // 8 bits counter
                            rx_packet.rx_buf[current_byte_position] = counter;
                            current_byte_position++;
                            rx_packet.rx_buf[current_byte_position] = counter + 1;
                            current_byte_position++;
                        }
                    counter++;
                    if (counter == max_count)
                        {
                            counter = 0;
                        }
                }
            fifo_buff.push(rx_packet);
            wake_udp_thread.notify_one();
            std::cout << "." << std::flush;
            usleep(100000);
        }
    // Send a few zero-length packets to signal receiver we are done
    boost::array<char, 0> send_buf;
    for (int i = 0; i < 3; i++)
        d_socket->send_to(boost::asio::buffer(send_buf), d_endpoint);

    std::cout << "Joining threads...\n";
    send_samples = false;
    wake_udp_thread.notify_one();
    udp_thread.join();

    d_socket->close();
    delete d_socket;
    return 0;
}
