/* -*- c++ -*- */
/*
Simple utility that checks the consistency of the received stream of int32_t (4bytes) data
generated in the FPGA by a binary counter.
Coded by Javier Arribas (jarribas@cttc.es)
 */
#include <assert.h>
#include <cstring>
#include <fcntl.h>
#include <iostream>
#include <queue>
#include <signal.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

static volatile bool send_samples;

#define NUM_PACKETS 100000
#define RX_PACKET_SIZE_BYTES 4096

void intHandler(int dummy __attribute__((unused)))
{
    send_samples = false;
    std::cout << "Sent notification to stop all threads\n";
}

void print_throughput(struct timespec *tick, struct timespec *tock)
{
    double start, end, diff, bytes_per_sec;
    double numBytes = (double)NUM_PACKETS * RX_PACKET_SIZE_BYTES;
    start = tick->tv_sec + tick->tv_nsec / 1e9;
    end = tock->tv_sec + tock->tv_nsec / 1e9;
    diff = end - start;

    bytes_per_sec = numBytes / (double)(1 << 20) / diff;

    printf("Received %d %d-byte packets in %.9f sec: %.3f MB/s\n",
        NUM_PACKETS, RX_PACKET_SIZE_BYTES, diff, bytes_per_sec);
}


int main(int argc, char **argv)
{
    signal(SIGINT, intHandler);
    //open the EZDMA file descriptor mapped to the device
    int rx_fd = open("/dev/loop_rx", O_RDONLY);
    if (rx_fd < 0)
        {
            std::cout << "can't open loop device\n";
            return 0;
        }

    send_samples = true;

    // Main loop
    uint32_t counter_buffer[RX_PACKET_SIZE_BYTES / 4];
    memset((uint8_t *)counter_buffer, 0, RX_PACKET_SIZE_BYTES);
    uint32_t last_count;
    bool first_packet = true;
    long int diff;
    std::queue<int> diff_queue;
    struct timespec tick, tock;
    int n_flush = 1000;
    for (int m = 0; m < n_flush; m++)
        {
            read(rx_fd, (uint8_t *)&counter_buffer[0], RX_PACKET_SIZE_BYTES);
        }
    printf("Receiving %d %d-byte packets\n", NUM_PACKETS, RX_PACKET_SIZE_BYTES);
    uint32_t n = 0;
    while (send_samples == true and n < NUM_PACKETS)
        {
            read(rx_fd, (uint8_t *)counter_buffer, RX_PACKET_SIZE_BYTES);
            if (first_packet == true)
                {
                    assert(!clock_gettime(CLOCK_MONOTONIC, &tick));
                    last_count = counter_buffer[0];
                    first_packet = false;
                }
            else
                {
                    diff = counter_buffer[0] - last_count;
                    if (abs(diff) != (1024))
                        {
                            diff_queue.push(diff);
                            std::cout << "o" << std::flush;
                        }
                    last_count = counter_buffer[0];
                }
            n++;
        }

    assert(!clock_gettime(CLOCK_MONOTONIC, &tock));
    print_throughput(&tick, &tock);
    close(rx_fd);

    std::cout << "Error packets report:\n";
    if (diff_queue.empty())
        {
            std::cout << "No Errors! All packets receiver OK!\n";
        }
    else
        {
            while (!diff_queue.empty())
                {
                    std::cout << " Diff:" << diff_queue.front() << "\n";
                    diff_queue.pop();
                }
        }
    //std::cout<<"Last buffer: \n";
    //for(int n=1;n<(RX_PACKET_SIZE_BYTES/4);n++)
    //{
    //if ((counter_buffer[n]-counter_buffer[n-1])!=1)
    //{
    //	std::cout<<"buf["<<n<<"]-buff["<<n-1<<"]: "<<counter_buffer[n]-counter_buffer[n-1]<<std::endl;
    //}
    //std::cout<<"buf["<<n<<"]: "<<counter_buffer[n]<<std::endl;
    //}
    return 0;
}
