#include <boost/shared_ptr.hpp>
#include <gnuradio/blocks/char_to_float.h>
#include <gnuradio/blocks/deinterleave.h>
#include <gnuradio/blocks/file_sink.h>
#include <gnuradio/blocks/float_to_complex.h>
#include <gnuradio/blocks/udp_source.h>
#include <gnuradio/top_block.h>
#include <signal.h>
#include <string>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>

static volatile bool send_samples;
gr::top_block_sptr top_block_;

void intHandler(int dummy __attribute__((unused)))
{
    send_samples = false;
}

int main(int argc, char **argv)
{
    signal(SIGINT, intHandler);

    // config
    int RF_channels_ = 1;

    //smart pointers to gnuradio blocks
    gr::blocks::udp_source::sptr udp_source_;
    gr::blocks::deinterleave::sptr demux_;
    std::vector<boost::shared_ptr<gr::block>> file_sink;
    std::vector<boost::shared_ptr<gr::block>> char_to_float;
    std::vector<boost::shared_ptr<gr::block>> float_to_complex_;

    //gnuradio block instantiation
    udp_source_ = gr::blocks::udp_source::make(sizeof(char), "0.0.0.0", 1234, 1024, true);
    demux_ = gr::blocks::deinterleave::make(sizeof(char), 1);
    //create I, Q -> gr_complex type conversion blocks
    for (int n = 0; n < (RF_channels_ * 2); n++)
        {
            char_to_float.push_back(gr::blocks::char_to_float::make());
        }

    for (int n = 0; n < RF_channels_; n++)
        {
            float_to_complex_.push_back(gr::blocks::float_to_complex::make());
            file_sink.push_back(gr::blocks::file_sink::make(sizeof(gr_complex), ("udp_rx_ch" + std::to_string(n) + ".dat").c_str()));
        }

    top_block_ = gr::make_top_block("GNSSFlowgraph");

    top_block_->connect(udp_source_, 0, demux_, 0);
    for (int n = 0; n < (RF_channels_ * 2); n++)
        {
            top_block_->connect(demux_, n, char_to_float.at(n), 0);
        }
    for (int n = 0; n < RF_channels_; n++)
        {
            top_block_->connect(char_to_float.at(n * 2), 0, float_to_complex_.at(n), 0);
            top_block_->connect(char_to_float.at(n * 2 + 1), 0, float_to_complex_.at(n), 1);
            top_block_->connect(float_to_complex_.at(n), 0, file_sink.at(n), 0);
        }

    std::cout << "Start flowgraph\n";
    send_samples = true;
    top_block_->start();
    std::cout << "flowgraph running\n";
    while (send_samples)
        {
            usleep(100000);
        }
    std::cout << "Stopping flowgraph\n";
    top_block_->stop();
    top_block_->disconnect_all();
    return 0;
}
