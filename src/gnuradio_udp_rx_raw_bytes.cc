#include <boost/shared_ptr.hpp>
#include <gnuradio/blocks/file_sink.h>
#include <gnuradio/blocks/skiphead.h>
#include <gnuradio/blocks/udp_source.h>
#include <gnuradio/top_block.h>
#include <signal.h>
#include <string>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>

static volatile bool send_samples;
gr::top_block_sptr top_block_;

void intHandler(int dummy __attribute__((unused)))
{
    send_samples = false;
}

int main(int argc, char **argv)
{
    signal(SIGINT, intHandler);

    if (argc < 5)
        {
            std::cout << "usage: gnuradio_udp_rx_raw_bytes [output_file] [local_ip] [udp_port] [payload_size_bytes]\n";
            return 0;
        }
    std::string output_file(argv[1]);
    std::string host(argv[2]);
    int port = atoi(argv[3]);
    int payload_size_bytes = atoi(argv[4]);

    //smart pointers to gnuradio blocks
    gr::blocks::udp_source::sptr udp_source_;
    gr::blocks::file_sink::sptr file_sink;
    gr::blocks::skiphead::sptr skip_head;

    //gnuradio block instantiation
    udp_source_ = gr::blocks::udp_source::make(sizeof(char), host, port, payload_size_bytes, true);
    file_sink = gr::blocks::file_sink::make(sizeof(char), output_file.c_str());
    skip_head = gr::blocks::skiphead::make(sizeof(char), 40000000);  //it has to be multiple of 4 bytes
    top_block_ = gr::make_top_block("GNSSFlowgraph");
    top_block_->connect(udp_source_, 0, skip_head, 0);
    top_block_->connect(skip_head, 0, file_sink, 0);
    std::cout << "Start flowgraph\n";
    send_samples = true;
    top_block_->start();
    std::cout << "flowgraph running\n";
    while (send_samples)
        {
            usleep(100000);
        }
    std::cout << "Stopping flowgraph\n";
    top_block_->stop();
    top_block_->disconnect_all();
    return 0;
}
