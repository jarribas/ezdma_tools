/* -*- c++ -*- */
/*

 */
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/format.hpp>
#include <boost/lockfree/spsc_queue.hpp>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <queue>
#include <signal.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <thread>

static volatile bool send_samples;

std::mutex udp_thread_mutex;
std::condition_variable wake_udp_thread;

#define RX_PACKET_SIZE_BYTES 4096

class ezdma_packet
{
public:
    uint8_t rx_buf[RX_PACKET_SIZE_BYTES];
};

boost::lockfree::spsc_queue<ezdma_packet, boost::lockfree::capacity<256> >
    fifo_buff;

void intHandler(int dummy __attribute__((unused)))
{
    send_samples = false;
    wake_udp_thread.notify_one();
    std::cout << "Sent notification to stop all threads\n";
}

void send_udp_packet(boost::asio::ip::udp::socket *d_socket,
    boost::asio::ip::udp::endpoint d_endpoint,
    int udp_packet_size_bytes)
{
    // ssize_t r;
    ezdma_packet signal_samples;
    uint8_t remaining_buffer[RX_PACKET_SIZE_BYTES * 2];  // storage for the
                                                         // remaining bytes, to be
                                                         // sent in the next batch
    int remaining_bytes = 0;
    while (send_samples)
        {
            std::unique_lock<std::mutex> lk(udp_thread_mutex);
            wake_udp_thread.wait(
                lk, [] { return !fifo_buff.empty() or send_samples == false; });

            fifo_buff.pop(signal_samples);  // new sample buffer is ready to be sent
            lk.unlock();
            if (send_samples == false) break;

            // send the UDP packet
            try
                {
                    // save the new bytes in the buffer
                    memcpy(&remaining_buffer[remaining_bytes], &signal_samples.rx_buf[0],
                        RX_PACKET_SIZE_BYTES);
                    remaining_bytes += RX_PACKET_SIZE_BYTES;
                    int buffer_pointer = 0;
                    while (remaining_bytes >= udp_packet_size_bytes)
                        {
                            d_socket->send_to(
                                boost::asio::buffer((void *)&remaining_buffer[buffer_pointer],
                                    udp_packet_size_bytes),
                                d_endpoint);
                            // if (r!=udp_packet_size_bytes)
                            //{
                            //	std::cout<<"UDP is not sending all the bytes requested in a
                            //single packet!\n";
                            //	break;
                            //}
                            remaining_bytes -= udp_packet_size_bytes;
                            buffer_pointer += udp_packet_size_bytes;
                        }
                    // reorder the remaining bytes to the beginning of the buffer (todo:
                    // replace it with a circular buffer)
                    memmove(&remaining_buffer[0], &remaining_buffer[buffer_pointer],
                        remaining_bytes);

                    // std::cout<<" buffer_pointer: "<<buffer_pointer<<"
                    // remaining_bytes:"<<remaining_bytes<<std::endl;
                }
            catch (std::exception &e)
                {
                    std::cout << boost::format("send error: %s") % e.what() << std::endl;
                    break;
                }
        }
}

int main(int argc, char **argv)
{
    signal(SIGINT, intHandler);

    if (argc < 4)
        {
            std::cout << "usage: ezdma_send_udp [destination_ip] [udp_port] "
                         "[udp_packet_size_bytes]\n";
            return 0;
        }
    std::string host(argv[1]);
    std::string s_port(argv[2]);
    int udp_packet_size_bytes = atoi(argv[3]);

    // open the EZDMA file descriptor mapped to the device
    int rx_fd = open("/dev/loop_rx", O_RDONLY);
    if (rx_fd < 0)
        {
            std::cout << "can't open loop device\n";
            return 0;
        }

    // open the UDP socket
    boost::asio::ip::udp::socket *d_socket;
    boost::asio::ip::udp::endpoint d_endpoint;
    boost::asio::ip::udp::endpoint d_endpoint_rcvd;
    boost::asio::io_service d_io_service;
    boost::asio::ip::udp::resolver resolver(d_io_service);
    boost::asio::ip::udp::resolver::query query(
        host, s_port, boost::asio::ip::resolver_query_base::passive);
    d_endpoint = *resolver.resolve(query);
    d_socket = new boost::asio::ip::udp::socket(d_io_service);
    d_socket->open(d_endpoint.protocol());
    boost::asio::socket_base::reuse_address roption(true);
    d_socket->set_option(roption);
    boost::asio::socket_base::broadcast option(true);
    d_socket->set_option(option);
    send_samples = true;

    // Prepare the sender thread
    // start pcap capture thread
    std::thread udp_thread;
    udp_thread = std::thread(&send_udp_packet, d_socket, d_endpoint,
        udp_packet_size_bytes);

    // Main loop
    ezdma_packet rx_packet;
    while (send_samples == true)
        {
            read(rx_fd, rx_packet.rx_buf, RX_PACKET_SIZE_BYTES);
            fifo_buff.push(rx_packet);
            wake_udp_thread.notify_one();
        }
    // Send a few zero-length packets to signal receiver we are done
    boost::array<char, 0> send_buf;
    for (int i = 0; i < 3; i++)
        d_socket->send_to(boost::asio::buffer(send_buf), d_endpoint);

    std::cout << "Joining threads...\n";
    send_samples = false;
    wake_udp_thread.notify_one();
    udp_thread.join();
    close(rx_fd);
    d_socket->close();
    delete d_socket;
    return 0;
}
