#include "gr_complex_ip_packet_source.h"
#include <boost/filesystem.hpp>
#include <boost/shared_ptr.hpp>
#include <gnuradio/blocks/complex_to_float.h>
#include <gnuradio/blocks/file_sink.h>
#include <gnuradio/blocks/float_to_char.h>
#include <gnuradio/blocks/interleave.h>
#include <gnuradio/blocks/null_sink.h>
#include <gnuradio/blocks/skiphead.h>
#include <gnuradio/top_block.h>
#include <signal.h>
#include <string>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>

static volatile bool send_samples;
gr::top_block_sptr top_block_;

void intHandler(int dummy __attribute__((unused))) { send_samples = false; }

int main(int argc, char **argv)
{
    signal(SIGINT, intHandler);

    if (argc < 11)
        {
            std::cout << "usage: gnuradio_udp_rx_gr_complex"
                      << " [output_file]"
                      << " [eth_device]"
                      << " [udp_sender_ip]"
                      << " [udp_port]"
                      << " [payload_size_bytes]"
                      << " [wire_sample_type]"
                      << " [n_channels_in_wire]"
                      << " [num_channels_to_save]"
                      << " [IQ_swap]"
                      << " [output_format]\n";
            return 0;
        }
    std::string output_file(argv[1]);
    std::size_t found = output_file.find_last_of("/\\");
    std::string path_str = ".";
    if (found != std::string::npos)
        {
            path_str = output_file.substr(0, found);
            std::cout << "Output file path: " << path_str << std::endl;
        }

    std::string eth_device(argv[2]);
    std::string sender_ip(argv[3]);
    int port = atoi(argv[4]);
    int payload_size_bytes = atoi(argv[5]);
    std::string wire_sample_type(argv[6]);
    int n_channels_in_wire = atoi(argv[7]);
    int n_channels_to_save = atoi(argv[8]);
    bool IQ_swap = atoi(argv[9]) == 1;
    std::string output_format(argv[10]);

    // smart pointers to gnuradio blocks
    gr_complex_ip_packet_source::sptr udp_source_;

    // gnuradio block instantiation
    size_t source_item_size = sizeof(gr_complex);
    udp_source_ = gr_complex_ip_packet_source::make(
        eth_device, sender_ip, port, payload_size_bytes, n_channels_in_wire,
        wire_sample_type, source_item_size, IQ_swap);

    std::vector<boost::shared_ptr<gr::block>> type_converter_cpx_to_float_vector;
    std::vector<boost::shared_ptr<gr::block>> type_converter_float_to_char_vector;
    std::vector<boost::shared_ptr<gr::block>> char_interleaver_vector;

    size_t file_output_item_size = sizeof(gr_complex);
    if (output_format.compare("cbyte") == 0)
        {
            for (int n = 0; n < n_channels_to_save; n++)
                {
                    // one complex to float per channel
                    type_converter_cpx_to_float_vector.push_back(
                        gr::blocks::complex_to_float::make());
                    // two float to chart per channel
                    type_converter_float_to_char_vector.push_back(
                        gr::blocks::float_to_char::make());
                    type_converter_float_to_char_vector.push_back(
                        gr::blocks::float_to_char::make());
                    // one interleaver per channel
                    char_interleaver_vector.push_back(
                        gr::blocks::interleave::make(sizeof(char)));
                }

            file_output_item_size = sizeof(char);
        }
    std::vector<boost::shared_ptr<gr::block>> file_sink_vector;

    for (int n = 0; n < n_channels_to_save; n++)
        {
            file_sink_vector.push_back(gr::blocks::file_sink::make(
                file_output_item_size,
                (output_file + "_ch" + std::to_string(n) + ".bin").c_str()));
        }

    top_block_ = gr::make_top_block("GNSSFlowgraph");

    if (output_format.compare("cbyte") == 0)
        {
            std::cout << " Output recording will be interleaved bytes" << std::endl;
            for (int n = 0; n < n_channels_to_save; n++)
                {
                    top_block_->connect(udp_source_, n,
                        type_converter_cpx_to_float_vector.at(n), 0);

                    top_block_->connect(type_converter_cpx_to_float_vector.at(n), 0,
                        type_converter_float_to_char_vector.at(n * 2), 0);
                    top_block_->connect(type_converter_cpx_to_float_vector.at(n), 1,
                        type_converter_float_to_char_vector.at(n * 2 + 1), 0);

                    top_block_->connect(type_converter_float_to_char_vector.at(n * 2), 0,
                        char_interleaver_vector.at(n), 0);
                    top_block_->connect(type_converter_float_to_char_vector.at(n * 2 + 1), 0,
                        char_interleaver_vector.at(n), 1);

                    top_block_->connect(char_interleaver_vector.at(n), 0,
                        file_sink_vector.at(n), 0);
                }
        }
    else
        {
            std::cout << " Output recording will be gr_complex" << std::endl;
            for (int n = 0; n < n_channels_to_save; n++)
                {
                    top_block_->connect(udp_source_, n, file_sink_vector.at(n), 0);
                }
        }
    std::cout << "Start flowgraph\n";
    send_samples = true;
    top_block_->start();
    std::cout << "flowgraph running, recording samples...\n";

    boost::filesystem::space_info si;
    double old_free_space_gb = 0.0;
    double free_space_gb;
    long int capture_time_s = 0;
    // std::streamsize ss = std::cout.precision();  // save current precision
    std::cout.setf(std::ios::fixed, std::ios::floatfield);
    std::setprecision(2);
    try
        {
            while (send_samples)
                {
                    si = boost::filesystem::space(path_str);
                    free_space_gb = static_cast<double>(si.available) / 1e9;
                    if (old_free_space_gb != 0)
                        {
                            std::cout << "Cap time: " << static_cast<double>(capture_time_s) / 60.0
                                      << " [mins], free space: " << free_space_gb << " [GBytes] "
                                      << ", available recording time: "
                                      << (free_space_gb / (old_free_space_gb - free_space_gb)) /
                                             60.0
                                      << " [mins]";
                        }
                    old_free_space_gb = free_space_gb;
                    std::flush(std::cout);
                    sleep(1);
                    capture_time_s++;
                    std::cout << '\r';
                }
        }
    catch (const std::exception &ex)
        {
        }
    std::cout << "Stopping flowgraph\n";
    try
        {
            top_block_->stop();
            top_block_->disconnect_all();
        }
    catch (const std::exception &ex)
        {
        }

    return 0;
}
